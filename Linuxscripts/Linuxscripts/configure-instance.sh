#!/bin/sh
INSTANCE_DIR=/usr/share/jbossas-instances
STANDALONE_DIR=/var/lib/jbossas/standalone
JBOSS_HOME=/usr/share/jbossas

while getopts ":f:h" optname
  do
    case "$optname" in
	  "h")
        echo "-f provide the filename of the property"
		;;
      "f")
        echo "Filename property file is $OPTARG"
        propertyfile=$OPTARG  
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 1  
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 1
		;;
      *)
        echo "Unknown error while processing options"
        exit 1
		;;
    esac
done

if [ -z "$propertyfile" ]; then
    echo "Property file not set"
    exit 1
fi

. $propertyfile

echo "Start configuring instance..."
#Creating environment variable
$JBOSS_HOME/bin/jboss-cli.sh --connect --controller=0.0.0.0:$port --file=configure-instance.cli --properties=$propertyfile
echo "Stop configuring instance..."