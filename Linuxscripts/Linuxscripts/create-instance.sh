#!/bin/sh

INSTANCE_DIR=/usr/share/jbossas-instances
STANDALONE_DIR=/var/lib/jbossas/standalone
JDBC_DRIVER=/usr/share/jbossas/modules/com/microsoft/sqlserver
JBOSS_HOME=/usr/share/jbossas

while getopts ":n:p:h" optname
  do
    case "$optname" in
	  "h")
        echo "-n provide name of the instance"
		echo "-p provide port-offset of the instance"
        ;;
      "n")
        echo "Name instance is $OPTARG"
        INSTANCE_NAME=$OPTARG  
        ;;
      "p")
        echo "Port offset is $OPTARG"
        PORT_OFFSET=$OPTARG
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 1  
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 1
		;;
      *)
        echo "Unknown error while processing options"
        exit 1
		;;
    esac
done

if [ -z "$INSTANCE_NAME" ]; then
    echo "Name instance not set"
    exit 1
fi


if [ -z "$PORT_OFFSET" ]; then
    echo "Port offset not set"
    exit 1
fi

if [ ! -d "$INSTANCE_DIR" ]; then
  echo "Create instances directory $INSTANCE_DIR"
  mkdir $INSTANCE_DIR
fi

if [ -d "$INSTANCE_DIR/$INSTANCE_NAME" ]; then
  echo "Instance $INSTANCE_DIR/$INSTANCE_NAME already exists!"
  exit 1
fi

echo ">>>Start creating instance..."
echo ">>>Copy standalone directory..."
cp -r -L $STANDALONE_DIR $INSTANCE_DIR/$INSTANCE_NAME
chown -R jboss:jboss $INSTANCE_DIR/$INSTANCE_NAME
echo "<<<Copy standalone directory done"

echo ">>>Create link to jbossas service (/etc/init.d/jbossas)..."
ln -s /etc/init.d/jbossas /etc/init.d/jbossas-$INSTANCE_NAME
echo "<<<Create link to jbossas service done"

echo ">>>Copy service specific configuration file (/etc/sysconfig/jbossas)..."
cp /etc/sysconfig/jbossas /etc/sysconfig/jbossas-$INSTANCE_NAME
chown -R jboss:jboss /etc/sysconfig/jbossas-$INSTANCE_NAME

echo "JBOSSSH=/usr/share/jbossas/bin/$INSTANCE_NAME.sh

#Location to keep the console log
JBOSS_CONSOLE_LOG=/var/log/jbossas/$INSTANCE_NAME/console.log

#JAVA_OPTS variable
JAVA_OPTS=\"-Xms1303m -Xmx1303m -XX:MaxPermSize=256m -Djboss.server.base.dir=/usr/share/jbossas-instances/$INSTANCE_NAME -Djboss.server.log.dir=/var/log/jbossas/$INSTANCE_NAME -Djboss.server.config.dir=/usr/share/jbossas-instances/$INSTANCE_NAME/configuration -Djboss.bind.address.management=0.0.0.0 -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=$JBOSS_MODULES_SYSTEM_PKGS -Djava.awt.headless=true -Djboss.bind.address=0.0.0.0 -Djboss.socket.binding.port-offset=$PORT_OFFSET\"" >> /etc/sysconfig/jbossas-$INSTANCE_NAME
echo "<<<Copy service specific configuration file done"

echo ">>>Create JBoss admin user..."
export JAVA_OPTS="-Djboss.server.config.user.dir=$INSTANCE_DIR/$INSTANCE_NAME/configuration -Djboss.domain.config.user.dir=$INSTANCE_DIR/$INSTANCE_NAME/configuration"
/usr/share/jbossas/bin/add-user.sh -u admin -p 2*4=eight
unset JAVA_OPTS
echo "<<<Create JBoss admin user done"

echo ">>>Add service to startup..."
/sbin/chkconfig --add jbossas-$INSTANCE_NAME
/sbin/chkconfig jbossas-$INSTANCE_NAME on
echo "<<<Add service to startup done"

echo ">>>Create log directory..."
mkdir /var/log/jbossas/$INSTANCE_NAME
chown -R jboss:jboss /var/log/jbossas/$INSTANCE_NAME
rm -rf $INSTANCE_DIR/$INSTANCE_NAME/log
ln -s /var/log/jbossas/$INSTANCE_NAME/ $INSTANCE_DIR/$INSTANCE_NAME/log
chown -R jboss:jboss $INSTANCE_DIR/$INSTANCE_NAME/log
echo "<<<Create log directory done"

echo ">>>Copy startup script..."
cp /usr/share/jbossas/bin/standalone.sh /usr/share/jbossas/bin/$INSTANCE_NAME.sh
chown -R jboss:jboss /usr/share/jbossas/bin/$INSTANCE_NAME.sh
echo "<<<Copy startup script"

echo ">>>Configure JDBC Module..."
if [ ! -d "$JDBC_DRIVER" ]; then
  echo "Add JDBC module"
  $JBOSS_HOME/bin/jboss-cli.sh --connect --controller=0.0.0.0:$port --command="module add --name=com.microsoft.sqlserver --resources=/home/tlsadx96sj/sqljdbc4.jar --dependencies=javax.api,javax.transaction.api" --properties=$propertyfile
fi
echo "<<<Configure JDBC Module done"
echo "<<<Creating instance done"